import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  figureCreationDisable: boolean = true;

  canvasSizeInput: number = 500;
  canvasSize: number;

  xCoordinate: number = 0;
  yCoordinate: number = 0;
  radius: number = 0;

  @ViewChild('canvas', { static: true })
  canvas: ElementRef<HTMLCanvasElement>;
  private ctx: CanvasRenderingContext2D;

  xPoints: number[] = [];
  yPoints: number[] = [];

  enteredCoordinates: string = '';
  figurePerimeter: number = 0;
  figureArea: number = 0;

  ngOnInit(): void {
    this.ctx = this.canvas.nativeElement.getContext('2d');
  }

  createNewCanvas() {
    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.xPoints = [];
    this.yPoints = [];
    this.figurePerimeter = 0;
    this.figureArea = 0;
    this.enteredCoordinates = '';
    if (this.canvasSizeInput < 10) this.canvasSizeInput = 10;
    this.canvasSize = this.canvasSizeInput;
    this.ctx.beginPath();
  }

  addPoint() {
    this.xPoints.push(this.xCoordinate);
    this.yPoints.push(this.yCoordinate);
    this.enteredCoordinates += ` (${this.xCoordinate};${this.yCoordinate})`;
  }

  drawAndCalculatePolygon() {
    for (let i = 0; i < this.xPoints.length; i++) {
      if (i === 0) {
        this.ctx.moveTo(this.xPoints[i], this.yPoints[i]);
      } else {
        this.ctx.lineTo(this.xPoints[i], this.yPoints[i]);

        if ((i === this.xPoints.length - 1) && (this.xPoints[i] !== this.xPoints[0] || this.yPoints[i] !== this.yPoints[0])) {
          this.xPoints.push(this.xPoints[0]);
          this.yPoints.push(this.yPoints[0]);
          this.enteredCoordinates += ` (${this.xPoints[0]};${this.xPoints[0]})`;
        }

        this.figurePerimeter += Math.sqrt(Math.pow((this.xPoints[i] - this.xPoints[i - 1]), 2) + Math.pow((this.yPoints[i] - this.yPoints[i - 1]), 2));
        this.figureArea += this.xPoints[i - 1] * this.yPoints[i] - this.yPoints[i - 1] * this.xPoints[i];
      }
    }
    this.ctx.strokeStyle = "rgb(146, 0, 80)";
    this.ctx.fillStyle  = "rgb(228, 167, 200)";
    this.ctx.lineWidth = 3;
    this.ctx.stroke();
    this.ctx.fill();

    this.figureArea = Math.abs(this.figureArea / 2);
  }

  drawAndCalculateCircle() {
    this.ctx.strokeStyle = "rgb(146, 0, 80)";
    this.ctx.fillStyle  = "rgb(228, 167, 200)";
    this.ctx.lineWidth = 3;
    this.ctx.beginPath();
    this.ctx.arc(this.radius, this.radius, this.radius, 0, 2 * Math.PI, false);
    this.ctx.stroke();
    this.ctx.fill();

    this.figurePerimeter += 2 * Math.PI * this.radius;
    this.figureArea += Math.pow(this.figurePerimeter, 2);
  }
}
